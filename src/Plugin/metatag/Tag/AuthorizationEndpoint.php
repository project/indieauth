<?php

namespace Drupal\indieauth\Plugin\metatag\Tag;

use Drupal\metatag\Plugin\metatag\Tag\LinkRelBase;

/**
 * Provides a plugin for the 'authorization_endpoint' meta tag.
 *
 * @MetatagTag(
 *   id = "authorization_endpoint",
 *   label = @Translation("Authorization endpoint URL"),
 *   description = @Translation("Used for authorization endpoint URL with rel='authorization_endpoint' link."),
 *   name = "authorization_endpoint",
 *   group = "advanced",
 *   weight = 2,
 *   type = "uri",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class AuthorizationEndpoint extends LinkRelBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
