<?php

namespace Drupal\indieauth\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuth;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Indieauth routes.
 */
final class IndieauthController extends ControllerBase {

  const PROVIDER = 'indieauth';

  /**
   * The immutable config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cache service.
   *
   * @var Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache service.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The externalauth service.
   *
   * @var \Drupal\externalauth\ExternalAuth
   */
  protected $externalauth;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   * @param \Drupal\externalauth\ExternalAuth $externalauth
   *   The externalauth service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      RequestStack $request_stack,
      CacheBackendInterface $cache,
      Client $http_client,
      ExternalAuth $externalauth
  ) {
    $this->config = $config_factory->get('indieauth.settings');
    $this->requestStack = $request_stack;
    $this->cache = $cache;
    $this->httpClient = $http_client;
    $this->externalauth = $externalauth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('cache.default'),
      $container->get('http_client'),
      $container->get('externalauth.externalauth')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $request = $this->requestStack->getCurrentRequest();
    $authorization_code = $request->get('code');
    $me = $request->get('me');
    $state = $request->get('state');

    // Verify needed parameters are available.
    if (!$authorization_code
      || !$me
      || !$state) {
      return new JsonResponse([
        'error' => 'invalid_request',
        'error_description' => 'One or all of the parameters authorization_code, me ore state are missing.',
      ], 400);
    }

    // Verify state token.
    $cid = implode(':', ['indieauth', $me, $state]);
    $authorization = $this->cache->get($cid)->data;
    if ($state !== $authorization['state']) {
      return new JsonResponse([
        'error' => 'invalid_request',
        'error_description' => 'The state token is invalid.',
      ], 400);
    }

    // Verify authorization code.
    try {
      $response = $this->httpClient->post(
        $this->config->get('authorization_endpoint'), [
          'headers' => [
            'Accept' => 'application/json',
          ],
          'form_params' => [
            'code' => $authorization_code,
            'redirect_uri' => Url::fromRoute('indieauth.endpoint')->setAbsolute()->toString(),
            'client_id' => Url::fromRoute('<front>')->setAbsolute()->toString(),
          ],
        ]
      );

      $response = json_decode($response->getBody()->getContents());

      if ($this->config->get('register')) {
        $account_data['name'] = static::prepareUsername($response->me);
        $this->externalauth->loginRegister($response->me, static::PROVIDER, $account_data);
      }
      else {
        $this->externalauth->login($response->me, static::PROVIDER);
      }

      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }
    catch (\Exception $e) {
      return new JsonResponse([
        'error' => 'invalid_request',
        'error_description' => $e->getMessage(),
      ], 400);
    }
  }

  /**
   * Prepare username from authname.
   *
   * @param string $authname
   *   The indieauth authname.
   *
   * @return string
   *   The reformatted username.
   */
  private static function prepareUsername(string $authname) {
    $parts = parse_url($authname);
    $path_parts = explode('/', $parts['path']);
    $string = implode('-', [$parts['host'], end($path_parts)]);
    return str_replace(['.', '/'], ['-', '-'], $string);
  }

}
