<?php

namespace Drupal\indieauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Indieauth settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'indieauth_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['indieauth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['authorization_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization endpoint'),
      '#default_value' => $this->config('indieauth.settings')->get('authorization_endpoint'),
    ];
    $form['register'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow registration'),
      '#default_value' => $this->config('indieauth.settings')->get('register'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('authorization_endpoint'))
      && filter_var($form_state->getValue('authorization_endpoint'), FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName('authorization_endpoint', $this->t('The authorization endpoint must be a valid URL.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('indieauth.settings')
      ->set('authorization_endpoint', $form_state->getValue('authorization_endpoint'))
      ->set('register', $form_state->getValue('register'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
