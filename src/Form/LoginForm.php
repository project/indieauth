<?php

namespace Drupal\indieauth\Form;

use Drupal\Component\Utility\Random;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use IndieAuth\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Indieauth form.
 */
final class LoginForm extends FormBase {

  /**
   * The cache service.
   *
   * @var Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Class constructor.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'indieauth_login';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['me'] = [
      '#type' => 'textfield',
      '#title' => t('Web Address'),
      '#size' => 15,
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('yourdomain.com'),
      ],
    ];
    $form['state'] = [
      '#type' => 'hidden',
      '#value' => (new Random())->name(12, TRUE),
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign In'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $me = trim($form_state->getValue('me'), '/');

    $scheme = parse_url($me, PHP_URL_SCHEME);
    if ($scheme !== 'https') {
      $form_state->setErrorByName('me', $this->t('Please prefix your domain with a secure protocol, e.g. <em>https:</em>//example.org'));
    }

    $authorization_endpoint = Client::discoverAuthorizationEndpoint($me);
    if (!$authorization_endpoint) {
      $form_state->setErrorByName('me', $this->t('Could not discover a valid IndieAuth authorization endpoint.'));
    }

    if ($authorization_endpoint) {
      // Cache the authorization details for later retrieval.
      $cid = implode(':', ['indieauth', $me, $form_state->getValue('state')]);
      $data = [
        'authorization_endpoint' => $authorization_endpoint,
      ];
      $this->cache->set($cid, $data, time() + 60 * 60);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $me = trim($form_state->getValue('me'), '/');
    $redirect_uri = Url::fromRoute('indieauth.endpoint')->setAbsolute()->toString();
    $client_id = Url::fromRoute('<front>')->setAbsolute()->toString();
    $state = $form_state->getValue('state');

    $cid = implode(':', ['indieauth', $me, $state]);
    $authorization = $this->cache->get($cid)->data;

    $authorization_url = Client::buildAuthorizationURL(
      $authorization['authorization_endpoint'],
      [
        'me' => $me,
        'redirect_uri' => $redirect_uri,
        'client_id' => $client_id,
        'state' => $state,
        // @todo This is optional.
        // 'prompt' => 'login',
      ]
    );

    // Cache the authorization details for later retrieval.
    $cid = implode(':', ['indieauth', $me, $state]);
    $authorization = [
      'me' => $me,
      'redirect_uri' => $redirect_uri,
      'client_id' => $client_id,
      'state' => $state,
    ] + $authorization;
    $this->cache->set($cid, $authorization, time() + 60 * 60);

    // Go to remote authorization url.
    $response = new TrustedRedirectResponse(Url::fromUri($authorization_url)->toString());
    $form_state->setResponse($response);
  }

}
